all: hello

hello: hello.o
	gcc -o $@ $^

hello.o: hello.c hello.h
	gcc -c -o $@ $<