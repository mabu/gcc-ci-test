/* simple hello world */

#include <stdio.h>
#include "hello.h"

int main(void)
{
    printf("%s\n", HELLO_MESSAGE);
    return 0;
}